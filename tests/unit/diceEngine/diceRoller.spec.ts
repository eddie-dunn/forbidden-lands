import { nbrSkulls, nbrSwords, rollDice, totalSwords } from "@/dice/diceRoller"
import { IRollResultPartial } from "src/dice/diceTypes"

describe(`${rollDice.name}`, () => {
  it(`no dice`, () => {
    const conf = {}
    const expected = {
      successes: 0,
      failBlack: 0,
      failWhite: 0,
      negativeBonus: false,
      rollLog: {
        black: [],
        blue: [],
        green: [],
        orange: [],
        red: [],
        white: [],
      },
    }
    const result = rollDice(conf)
    expect(result).toStrictEqual(expected)
  })
  // TODO: Add more tests if seeding RNG is enabled
})

describe(`${nbrSkulls.name}`, () => {
  it(`no skulls`, () => {
    const rolls: number[] = [2, 0, 0, 5, 2, 4]
    const expected = 0
    const result = nbrSkulls(rolls)
    expect(result).toBe(expected)
  })
  it(`1 skull`, () => {
    const rolls: number[] = [3, 1, 0, 6, 2, 4]
    const expected = 1
    const result = nbrSkulls(rolls)
    expect(result).toBe(expected)
  })
  it(`several skulls`, () => {
    const rolls: number[] = [1, 0, 1, 1, 8]
    const expected = 3
    const result = nbrSkulls(rolls)
    expect(result).toBe(expected)
  })
})

describe(`${nbrSwords.name}`, () => {
  it(`no successes`, () => {
    const rolls: number[] = [3, 1, 0, 5, 2, 4]
    const expected = 0
    const result = nbrSwords(rolls)
    expect(result).toBe(expected)
  })
  it(`1 success`, () => {
    const rolls: number[] = [3, 1, 0, 6, 2, 4]
    const expected = 1
    const result = nbrSwords(rolls)
    expect(result).toBe(expected)
  })
  it(`several successes`, () => {
    const rolls: number[] = [0, 7, 0, 12, 10, 8]
    const expected = 10
    const result = nbrSwords(rolls)
    expect(result).toBe(expected)
  })
})

describe(`${totalSwords.name}`, () => {
  it(`several successes`, () => {
    const rolls1: number[] = [3, 1, 0, 5, 2, 4]
    const rolls2: number[] = [3, 1, 0, 6, 2, 4]
    const rolls3: number[] = [0, 7, 0, 12, 10, 8]
    const rollResult: IRollResultPartial = {
      white: rolls1,
      red: rolls2,
      orange: rolls3,
    }
    const expected = 11
    const result = totalSwords(rollResult)
    expect(result).toBe(expected)
  })

  it(`several successes, negative bonus`, () => {
    const negativeRed = true
    const white: number[] = [6] // 1
    const red: number[] = [6, 6] // -2
    const orange: number[] = [12] // 4
    const expected = 3
    const rollResult: IRollResultPartial = { white, red, orange }
    const result = totalSwords(rollResult, negativeRed)
    expect(result).toBe(expected)
  })
})
