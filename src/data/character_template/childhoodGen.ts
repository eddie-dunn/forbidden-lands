import { childhoodAll } from "./childhood"

/** Use the code below to generate English translations for childhood.
 *
 * Usage:
 * ```
 * npx ts-node childhood.ts > childhoods_tmp.js
 * ```
 * Then add the childhood data to locales/en.ts
 */

let nameCollision = false
const formative_events: { [key: string]: string } = {}

for (const [profession, val] of Object.entries(childhoodAll)) {
  for (const event of val) {
    const nameKey = event.key
    const storyKey = event.key + " story"

    if (
      formative_events[nameKey] &&
      formative_events[storyKey] &&
      formative_events[storyKey] !== event.story
    ) {
      console.log(`${nameKey} already exists; will not add`)
      const newChildhoodNameKey = `${nameKey} ${profession}`
      console.log("Suggestion", { [nameKey]: newChildhoodNameKey })
      nameCollision = true
      continue
    } else {
      formative_events[nameKey] = event.key.replaceAll(/\[\w*\]/g, "").trim()
      formative_events[storyKey] = event.story
    }
  }
}

!nameCollision && console.log(formative_events)
