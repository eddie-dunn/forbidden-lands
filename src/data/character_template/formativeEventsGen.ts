import { formativeEventsAll } from "./formative_events"

/** Use the code below to generate English translations for formative events.
 *
 * Usage:
 * Uncomment code snippet below
 *
 * ```
 * npx ts-node formative_events.ts > tmp.js
 * ```
 * Then add the data to locales/en.ts
 */
let nameCollision = false
const formative_events: { [key: string]: string } = {}

for (const [profession, val] of Object.entries(formativeEventsAll)) {
  for (const event of val) {
    const nameKey = event.key
    const storyKey = event.key + " story"
    const itemKey = event.key + " items"

    if (formative_events[nameKey]) {
      console.log(`${nameKey} already exists; will not add`)
      const newChildhoodNameKey = `${nameKey} ${profession}`
      console.log("Suggestion", { [nameKey]: newChildhoodNameKey })
      nameCollision = true
      continue
    } else {
      formative_events[nameKey] = event.key.replaceAll(/\[\w*\]/g, "").trim()
      formative_events[storyKey] = event.story
      formative_events[itemKey] = event.items
    }
  }
}

!nameCollision && console.log(formative_events)
