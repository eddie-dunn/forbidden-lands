export type DiceColor =
  | "white"
  | "red"
  | "black"
  | "green"
  | "blue"
  | "orange"
  | "badred"

export interface IResultSummary {
  whiteSkulls?: number
  blackSkulls?: number
  success: number
}
